<?php

    require 'Ape.php';
    require 'Frog.php';

    $sheep = new Animal("shaun");

    echo $sheep->get_name() . "<br>"; // "shaun"
    echo $sheep->get_legs() . "<br>"; // 2
    echo $sheep->get_cold_blooded() . "<br>";// false
    echo "<br>";
    // NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())
    
    $sungokong = new Ape("kera sakti");
    $sungokong->yell(); // "Auooo"
    
    $kodok = new Frog("buduk");
    $kodok->jump() ; // "hop hop"